DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users (
  id BIGSERIAL NOT NULL PRIMARY KEY,
  name TEXT NOT NULL CHECK (length(name)<=30),
  surname TEXT NOT NULL CHECK (length(surname)<=30),
  avatar_url TEXT NOT NULL DEFAULT 'https://thesocietypages.org/socimages/files/2009/05/yammer.gif' CHECK (length(avatar_url)<=2000)
  );

DROP TABLE IF EXISTS tasks;
CREATE TABLE tasks (
  id BIGSERIAL NOT NULL PRIMARY KEY,
  title TEXT NOT NULL CHECK (length(title)<=200),
  content TEXT NOT NULL CHECK (length(content)<=2000),
  type TEXT NOT NULL CHECK (type='BUG' OR type='TASK' OR type='FEATURE'),
  priority NUMERIC(1) NOT NULL CHECK (priority >= 1 AND priority <= 5),
  severity NUMERIC(1) NOT NULL CHECK (severity >= 1 AND severity <= 3),
  user_id INTEGER REFERENCES users(id),
  status TEXT NOT NULL DEFAULT 'NEW' CHECK (status= 'NEW' OR status='ACTIVE' OR status='RESOLVED' OR status='CLOSED') ,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  activated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  resolved TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  closed TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
  );

INSERT INTO users (name, surname)
VALUES
    ('Milosz', 'Nowak'),
    ('Sebastian', 'Bednarczyk');

INSERT INTO tasks (title, content, type, priority, severity)
VALUES
    ('Zadanie 1', 'Przykladowa tresc zadania do wykonania.', 'TASK', 1, 1),
    ('Zadanie 2', 'Przykladowa tresc zadania do wykonania.', 'BUG', 1, 1),
    ('Zadanie 3', 'Przykladowa tresc zadania do wykonania.', 'BUG', 1, 1),
  	('Zadanie 4', 'Przykladowa tresc zadania do wykonania.', 'FEATURE', 1, 1),
    ('Zadanie 5', 'Przykladowa tresc zadania do wykonania.', 'TASK', 1, 1);