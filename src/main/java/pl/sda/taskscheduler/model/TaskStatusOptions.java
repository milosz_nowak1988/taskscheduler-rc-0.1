package pl.sda.taskscheduler.model;

public enum  TaskStatusOptions {

    NEW,
    ACTIVE,
    RESOLVED,
    CLOSED;
}
